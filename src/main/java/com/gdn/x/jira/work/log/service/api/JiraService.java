package com.gdn.x.jira.work.log.service.api;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.gdn.x.jira.work.log.utils.Constants;
import com.gdn.x.jira.work.log.utils.Utils;

import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.Issue.SearchResult;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.WorkLog;

@Service
public class JiraService {

	private JiraClient jiraClient;

	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(JiraService.class);

	public void downloadCsv(HttpServletResponse response, String csvReport) {
		response.setContentType("text/csv");
		String reportName = "jira-work-log-report.csv";

		response.setHeader("Content-disposition", "attachment;filename=" + reportName);
		try {
			response.getOutputStream().print(csvReport);
			response.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void initiateClient() throws Exception {
		BasicCredentials basicCreds = new BasicCredentials(Constants.USERNAME, Constants.PASSWORD);
		jiraClient = new JiraClient(Constants.JIRA_URL, basicCreds);
	}

	public String generateCsvReportByStartDateAndEndDate(SearchResult results, String startDate, String endDate)
			throws Exception {
		this.initiateClient();

		Map<String, Map<String, Double>> mapAssigneeAndWorkLog = new TreeMap<String, Map<String, Double>>();
		Set<String> dates = new TreeSet<String>();
		Iterator<Issue> it = results.iterator();
		while (it.hasNext()) {
			Issue issue = it.next();
			for (WorkLog workLog : issue.getAllWorkLogs()) {
				if (workLog.getStarted().after(Utils.dateFormatter.parse(startDate))
						&& workLog.getStarted().before(Utils.dateFormatter.parse(endDate))) {
					Map<String, Double> logWork = mapAssigneeAndWorkLog.get(workLog.getAuthor().getDisplayName());
					if (logWork == null) {
						logWork = new TreeMap<String, Double>();
						mapAssigneeAndWorkLog.put(workLog.getAuthor().getDisplayName(), logWork);
					}
					Double hours = logWork.get(Utils.dateFormatter.format(workLog.getStarted()));
					if (hours == null) {
						hours = 0.0;
					}
					hours += Utils.secToHour(workLog.getTimeSpentSeconds());
					logWork.put(Utils.dateFormatter.format(workLog.getStarted()), hours);
					dates.add(Utils.dateFormatter.format(workLog.getStarted()));
				}
			}
		}

		for (String assignee : mapAssigneeAndWorkLog.keySet()) {
			Map<String, Double> logWork = mapAssigneeAndWorkLog.get(assignee);
			for (String date : dates) {
				Double hours = logWork.get(date);
				if (hours == null) {
					logWork.put(date, 0.0);
				}
			}
		}

		logger.debug("mapAssigneeAndWorkLog : {}", mapAssigneeAndWorkLog.toString());
		System.out.println(mapAssigneeAndWorkLog.toString());
		return generateCsv(dates, mapAssigneeAndWorkLog);
	}

	public String generateCsvReport(SearchResult results) throws Exception {
		this.initiateClient();

		Map<String, Map<String, Double>> mapAssigneeAndWorkLog = new TreeMap<String, Map<String, Double>>();
		Set<String> dates = new TreeSet<String>();
		Iterator<Issue> it = results.iterator();
		while (it.hasNext()) {
			Issue issue = it.next();
			for (WorkLog workLog : issue.getAllWorkLogs()) {
				Map<String, Double> logWork = mapAssigneeAndWorkLog.get(workLog.getAuthor().getDisplayName());
				if (logWork == null) {
					logWork = new TreeMap<String, Double>();
					mapAssigneeAndWorkLog.put(workLog.getAuthor().getDisplayName(), logWork);
				}
				Double hours = logWork.get(Utils.dateFormatter.format(workLog.getStarted()));
				if (hours == null) {
					hours = 0.0;
				}
				hours += Utils.secToHour(workLog.getTimeSpentSeconds());
				logWork.put(Utils.dateFormatter.format(workLog.getStarted()), hours);
				dates.add(Utils.dateFormatter.format(workLog.getStarted()));
			}
		}

		for (String assignee : mapAssigneeAndWorkLog.keySet()) {
			Map<String, Double> logWork = mapAssigneeAndWorkLog.get(assignee);
			for (String date : dates) {
				Double hours = logWork.get(date);
				if (hours == null) {
					logWork.put(date, 0.0);
				}
			}
		}

		return generateCsv(dates, mapAssigneeAndWorkLog);
	}

	public SearchResult searchIssuesByJQL(final String jql) throws Exception {
		return jiraClient.searchIssues(jql);
	}

	public String generateCsv(Set<String> dates, Map<String, Map<String, Double>> mapAssigneeAndWorkLog)
			throws ParseException {
		StringBuilder sb = new StringBuilder();
		sb.append(Utils.csvOfSetOfString("", Utils.convertDateFormat(dates)));
		for (String assignee : mapAssigneeAndWorkLog.keySet()) {
			sb.append(Utils.csvOfSetOfDouble(assignee, mapAssigneeAndWorkLog.get(assignee).values()));
		}
		return sb.toString();
	}
}
