package com.gdn.x.jira.work.log.rest.web.controller.api;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gdn.x.jira.work.log.service.api.JiraService;
import com.gdn.x.jira.work.log.utils.Constants;
import com.gdn.x.jira.work.log.utils.Utils;
import com.wordnik.swagger.annotations.Api;

import net.rcarz.jiraclient.Issue.SearchResult;

@Controller
@Api(value = "Jira", description = "Jira Service API")
public class JiraController {

	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(JiraController.class);

	@Autowired
	JiraService jiraService;

	@RequestMapping(value = "/generateReportByStartDateAndEndDate", method = { RequestMethod.GET })
	public void generateReportByStartDateAndEndDate(HttpServletResponse response, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd")Date startDate,
			@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd")Date endDate) throws Exception {
		String jiraQuery = Utils.generateQuery(Utils.dateFormatter.format(startDate),
				Utils.dateFormatter.format(endDate), Constants.ASSIGNE_LIST);
		this.jiraService.initiateClient();
		SearchResult results = this.jiraService.searchIssuesByJQL(jiraQuery);
		String csvReport = this.jiraService.generateCsvReportByStartDateAndEndDate(results,
				Utils.dateFormatter.format(startDate), Utils.dateFormatter.format(endDate));
		this.jiraService.downloadCsv(response, csvReport);
	}

	@RequestMapping(value = "/generateReportByStartDateAndEndDateAndAssignees", method = { RequestMethod.GET })
	public void generateReportByStartDateAndEndDateAndAssignees(HttpServletResponse response,
			@RequestParam Date startDate, @RequestParam Date endDate, @RequestParam String assignees) throws Exception {
		String jiraQuery = Utils.generateQuery(Utils.dateFormatter.format(startDate),
				Utils.dateFormatter.format(endDate), assignees);
		this.jiraService.initiateClient();
		SearchResult results = this.jiraService.searchIssuesByJQL(jiraQuery);
		String csvReport = this.jiraService.generateCsvReportByStartDateAndEndDate(results,
				Utils.dateFormatter.format(startDate), Utils.dateFormatter.format(endDate));
		this.jiraService.downloadCsv(response, csvReport);
	}

	@RequestMapping(value = "/generateReportByJql", method = { RequestMethod.GET })
	public void generateReportByJql(HttpServletResponse response, @RequestParam String jiraQuery) throws Exception {
		this.jiraService.initiateClient();
		SearchResult results = this.jiraService.searchIssuesByJQL(jiraQuery);
		String csvReport = this.jiraService.generateCsvReport(results);
		this.jiraService.downloadCsv(response, csvReport);
	}
}
