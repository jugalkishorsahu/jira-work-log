package com.gdn.x.jira.work.log.utils;

public interface Constants {

	String JIRA_URL = "https://jira.gdn-app.com";
	String ASSIGNE_LIST = "(manisha.machhaiya, chakradhar.g, hardik.beladiya, shweta.shaw, megha.vaishy,"
			+ " jugal.k.sahu, manika.singh, arjita.mitra, hari.prasad, khyati.vaghamshi, krati.parakh)";
	String UPDATED_CLAUSE_LESS_THAN = "updated <= ";
	String UPDATED_CLAUSE_GREAT_THAN = "updated >= ";
	String CREATED_CLAUSE = "created <= ";
	String SEPARATOR = ",";
	String FILE_NAME = "/Users/jugalkishorsahu/Desktop/report.csv";
	String USERNAME = "";
	String PASSWORD = "";
}
