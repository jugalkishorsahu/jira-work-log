package com.gdn.x.jira.work.log.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class Utils {

	public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat displayFormatter = new SimpleDateFormat("dd MMM E");
	public static final DecimalFormat df = new DecimalFormat("#.00");
	public static final String AND = " and ";

	public static double secToHour(int sec) {
		return (double) sec / 3600;
	}

	public static String generateQuery(String startDate, String endDate, String assignees) {
	  
		return new StringBuilder().append("assignee was in ").append(assignees).append(Utils.AND)
				.append(Constants.UPDATED_CLAUSE_GREAT_THAN).append("'").append(startDate).append(" 00:00").append("'").append(Utils.AND).append(Constants.UPDATED_CLAUSE_LESS_THAN).append("'")
				.append(endDate).append(" 11:00").append("'").toString();
	}

	public static String csvOfSetOfString(String assignee, Set<String> values) throws ParseException {

		StringBuilder sb = new StringBuilder();
		sb.append(assignee);
		for (String value : values) {
			sb.append(Constants.SEPARATOR);
			sb.append(value);
		}
		sb.append("\n");
		return sb.toString();
	}

	public static String csvOfSetOfDouble(String assignee, Collection<Double> values) throws ParseException {

		StringBuilder sb = new StringBuilder();
		sb.append(assignee);
		for (Double value : values) {
			sb.append(Constants.SEPARATOR);
			sb.append(df.format(value));
		}
		sb.append("\n");
		return sb.toString();
	}

	public static Set<String> convertDateFormat(Set<String> dates) throws ParseException {
		Set<String> convertedDates = new LinkedHashSet<String>();
		for (String date : dates) {
			convertedDates.add(Utils.displayFormatter.format(Utils.dateFormatter.parse(date)));
		}
		return convertedDates;
	}
}
